package com.attlasian.test;
import org.testng.annotations.Test;
import com.attlasian.po.CreateIssuePo;
import com.attlasian.po.LoginPo;
import com.attlasian.po.UpdateIssuePo;

public class AttlasianTests {

@Test(description = "Create an issue")
	public void createissue() throws Exception {
		LoginPo lpo = new LoginPo();
		CreateIssuePo cpo = new CreateIssuePo();	
		lpo.setUp();
		cpo.create();
		lpo.afterTest();
	}

	@Test(description = "search and update an issue")
	public void searchupdateissue() throws Exception {
		LoginPo lpo = new LoginPo();
		UpdateIssuePo upo = new UpdateIssuePo();
		lpo.setUp();
		upo.updateissuePo();
		lpo.afterTest();
	}
}
