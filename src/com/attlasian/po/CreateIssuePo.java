package com.attlasian.po;

import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.*;

public class CreateIssuePo extends LoginPo {
	WebDriver driver;
	By create_lnk = By.id("create_link");
	By summary_txt = By.id("summary");
	By create_btn = By.id("create-issue-submit");
	public static final String dburl = "https://jira.atlassian.com/secure/Dashboard.jspa";

	public void create() throws Exception {
		driver = DriverClass.GetDriver().driver;
		int i = 0;
		String master = driver.getWindowHandle();
		driver.findElement(create_lnk).click();
		int timeCount = 1;
		do {
			driver.getWindowHandles();
			Thread.sleep(100);
			timeCount++;
			if (timeCount > 50) {
				break;
			}
		} while (driver.getWindowHandles().size() == 1);

		Set<String> handles = driver.getWindowHandles();
		for (String handle : handles) {
			if (!handle.equals(master)) {
				driver.switchTo().window(handle);
			}
		}
		driver.findElement(summary_txt).sendKeys("A New Test");
		driver.findElement(create_btn).click();
		while ((!driver.getCurrentUrl().equals(dburl)) && i < 10) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			i++;
		}
	}
}
