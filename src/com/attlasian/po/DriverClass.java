package com.attlasian.po;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class DriverClass {

	private static DriverClass instance = null;
	public WebDriver driver;

	public DriverClass() {
	}

	public static DriverClass GetDriver() {
		if (instance == null)
			instance = new DriverClass();

		return instance;
	}

	public void ResetDriver() {
		driver = new FirefoxDriver();
	}

}
