package com.attlasian.po;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.*;
public class UpdateIssuePo {
	WebDriver driver;
	By quicksearch = By.id("quickSearchInput");

	By desc = By.id("description");
	By editbutton = By.id("edit-issue");
	By editissue = By.id("edit-issue-submit");
	public static final String id = "TST-58040";
	int i = 0;

	public void updateissuePo() throws Exception {
		driver = DriverClass.GetDriver().driver;
		driver.findElement(quicksearch).sendKeys(id);
		driver.findElement(quicksearch).sendKeys(Keys.ENTER);
		while (!driver.getCurrentUrl().equals(
				"https://jira.atlassian.com/browse/TST-58040")
				&& i < 10) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			i++;
		}
		driver.findElement(editbutton).click();
		driver.findElement(desc).sendKeys("Edit Test");
		driver.findElement(editissue).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
}
