package com.attlasian.po;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.*;
public class LoginPo {
	WebDriver driver;
	public static final String url = "https://id.atlassian.com/login/login?application=jac&continue=https://jira.atlassian.com/browse/TST/?selectedTab=com.atlassian.jira.jira-projects-plugin:summary-panel";
	public static final String userid = "parag.wani@gmail.com";
	public static final String pwd = "Tester12345";
	public static final String BTN_LOGIN = "Tester12345";
	By username = By.id("username");
	By password = By.id("password");
	By btn_login = By.id("login-submit");

	@BeforeTest
	public void setUp() throws Exception {
		DriverClass.GetDriver().ResetDriver();
		driver = DriverClass.GetDriver().driver;
		int i = 0;

		driver.get(url);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(username).sendKeys(userid);
		Thread.sleep(1000);
		driver.findElement(password).sendKeys(pwd);
		driver.findElement(btn_login).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		while (driver.getCurrentUrl().equals(
				"https://jira.atlassian.com/secure/Dashboard.jspa")
				&& i < 10);
	}

	@AfterTest
	public void afterTest() {
		driver.close();
	}
}
